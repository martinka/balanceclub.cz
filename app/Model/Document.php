<?php


namespace App\Model;


use Latte\Engine;
use Nette\Bridges\ApplicationLatte\Template;

class Document
{

    protected $appDir;


    public function __construct($appDir)
    {
        $this->appDir = $appDir;
    }

    /**
     * @param Template $htmlTemplate
     * @param string $name
     * @param string $destination
     * @throws \Mpdf\MpdfException
     * @throws \Throwable
     */
    public function output($name = '', $destination = '')
    {
        $old_error_reporting = error_reporting(E_ALL & ~E_NOTICE); // mpdf errors

        $mpdf = new \Mpdf\Mpdf([
            'tempDir' => $this->appDir . '/../temp/cache/pdf',
            'mode' => 'utf-8',
            'format' => 'A4',
            'nonPrintMargin' => 0,
            'fontDir' => [
                __DIR__ . '/documentData/fonts'
            ],
            'fontdata' => [
                'khand' => [
                    'R' => 'Khand-Regular.ttf'
                ]
            ],
            'img_dpi' => 300,
            'dpi' => 300,
            'default_font' => 'khand',
            //'debug' => true,
            // Checks and reports on errors when parsing TTF files - adds significantly to processing time
            //'debugfonts' => true,
            'showImageErrors' => true,
            // Die and report error if table is too wide to contain whole words
            //'table_error_report' => true,
        ]);

        $mpdf->SetDisplayMode('fullpage');
        $mpdf->shrink_tables_to_fit = 1;
        $mpdf->keep_table_proportions = true;

        $mpdf->imageVars['logo'] = file_get_contents(__DIR__ . '/documentData/images/logo.png');

        $mpdf->DefHTMLHeaderByName('hlavicka', file_get_contents(__DIR__ . '/documentData/html/header.latte'));
        $mpdf->DefHTMLFooterByName('paticka', file_get_contents(__DIR__ . '/documentData/html/footer.latte'));

        //dump($htmlTemplate->__toString()); die();
        $mpdf->AddPageByArray([
            'margin-left' => 0,
            'margin-right' => 0,
            'margin-top' => 0,
            'margin-bottom' => 0,
            'margin-header' => 0,
            'margin-footer' => 0,
            //'odd-header-name' => 'hlavicka',
            //'odd-footer-name' => 'paticka',
            //'odd-header-value' => 1,
            //'odd-footer-value' => 1

        ]);
        $mpdf->WriteHTML($this->getHtmlTemplateString());
        $mpdf->Output($name, $destination);
    }


    /**
     * @return string
     * @throws \Throwable
     */
    protected function getHtmlTemplateString()
    {
        $template = new Template(new Engine());
        $template->setFile(__DIR__ . '/documentData/html/default.latte');

        return $template->__toString();
    }

}