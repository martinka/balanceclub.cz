<?php

namespace App\Presenters;

use Nette;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    public function actionDefault()
    {
        $dir = __DIR__ . '/templates/Homepage/';
        $files = array_diff(scandir($dir), array('..', '.'));
        $files2 = array_map(function ($f){
            return substr($f, 0,-6);
        }, $files);
        $this->template->files = $files2;
    }
}
