$(function() {

    var $header = $("header");
    var $fixedHeader = $('.header-fix');

    $('.menu-trigger', $header).add('.menu-trigger', $fixedHeader).on('click', function (e) {
        e.preventDefault();
        $header.addClass('menu-open');
    });

    $('.menu-close-trigger', $header).on('click', function (e) {
        e.preventDefault();
        $header.removeClass('menu-open');
    });

    $(".menu-responsive li a", $header).on('click', function (e) {
        if(!$(this).attr('href')) {
            e.preventDefault();
            $(this).parent().toggleClass('open');
        }
    });

    $(".slider-homepage").slick({
        arrows: false,
        autoplay: true
    });

    window.placeholderShownPolyfill.init();

    var $scheduleHeader = $(".schedule-header");

    var prevScrollpos = window.pageYOffset;
    $(window).on("scroll", function (e) {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos < currentScrollPos) {

            $header.removeClass('header-fixed-open');
            if($scheduleHeader.length)$scheduleHeader.addClass("schedule-header-small");
        } else {

            $header.addClass('header-fixed-open');
            if($scheduleHeader.length)$scheduleHeader.removeClass("schedule-header-small");
        }

        prevScrollpos = currentScrollPos;
    });

    $(".icon-settings", $scheduleHeader).on('click', function (e) {
        e.preventDefault();
        $scheduleHeader.removeClass("schedule-header-small");
        $header.addClass('header-fixed-open');
    });


    $('.show-password-btn').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('show-password-btn-visible').closest('.form-input').find('input').togglePassword();
    });


    $('.slider-simple .slides').slick({
        arrows: true,
        autoplay: true,
        fade: true,
        prevArrow: '<a class="prev"><span></span></a>',
        nextArrow: '<a class="next"><span></span></a>'
    });

});