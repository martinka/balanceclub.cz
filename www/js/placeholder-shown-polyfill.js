//==================================================
// Polyfill for pseudo selector :placeholder-shown
// Author: Subash Selvaraj(github userid:sesubash)
//==================================================
(function(){

  var placeholderShownPolyfill = {
    init: function() {
      var self = this;
      var els = document.querySelectorAll('[placeholder]');
      for (var i = 0; i < els.length; i++) {
          (function (el) {
              if (el.getAttribute('placeholder') != "")
                  self.updatePlaceholder.call(el);

              el.addEventListener('change', self.updatePlaceholder);
              el.addEventListener('keyup', self.updatePlaceholder);
              el.addEventListener('focus', function () {
                  self.updatePlaceholder.call(el, false)
              });
              el.addEventListener('blur', self.updatePlaceholder);
          }(els[i]))
      }
    },

    updatePlaceholder: function(add) {
      if(typeof add === "boolean"){
          this.parentNode.classList[add ? 'add' : 'remove']('placeholder-shown');
      } else {
          this.parentNode.classList[this.value ? 'remove' : 'add']('placeholder-shown');
      }
    }

  };

  if (/^[c|i]|d$/.test(document.readyState || "")) {
    placeholderShownPolyfill.init();
  } else {
    document.addEventListener("DOMContentLoaded", placeholderShownPolyfill.init());
  }

  window.placeholderShownPolyfill = placeholderShownPolyfill;

})();
